package main

import (
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"gitlab.com/everest-code/golib/log"
	"gitlab.com/everest-code/omni-auth/database"
	"gitlab.com/everest-code/omni-auth/handlers"
)

var (
	logger *log.Logger
	server *http.Server
)

func migrate(f *os.File) {
	defer func() {
		if err := recover().(error); err != nil {
			if err.Error() == "runtime error: invalid memory address or nil pointer dereference" {
				logger.Debug("Migration ommited")
			} else {
				logger.Panic("%s", err)
			}
		}
	}()

	mig, err := database.LoadMigrationFile(f)
	logger.Info("Migrating data")
	if err != nil {
		logger.Panic(err.Error())
	}

	rh := new(database.RoleHandler)
	if err := database.InitDatabase(rh); err != nil {
		logger.Panic(err.Error())
	}

	ph := new(database.PermissionHandler)
	if err := database.InitDatabase(ph); err != nil {
		logger.Panic(err.Error())
	}

	roleData, err := rh.GetAll()
	if err != nil {
		logger.Panic(err.Error())
	}

	permData, err := ph.GetAll()
	if err != nil {
		logger.Panic(err.Error())
	}

	if len(roleData) != 0 || len(permData) != 0 {
		logger.Warn("Migration update is not supported yet")
		logger.Panic("Database already migrated, please migrate the new configuration manually")
	}

	hasErrors := false

	for _, perm := range mig.ToPerms() {
		logger.Debug("Migrating permission '%s'", perm.String())
		err := ph.Create(&perm)
		if err != nil {
			hasErrors = true
			logger.Error("migrating permissions: %s", err.Error())
		}
	}

	for _, role := range mig.ToRoles() {
		logger.Debug("Migrating role '%s'", role.String())
		err := rh.Create(&role)
		if err != nil {
			hasErrors = true
			logger.Error("migrating roles: %s", err.Error())
		}
	}

	if hasErrors {
		os.Exit(1)
	} else {
		logger.Info("Migration finished")
	}
}

func createRouter(cors []string, conf *Conf) http.Handler {
	ud := new(database.UserHandler)
	if err := database.InitDatabase(ud); err != nil {
		logger.Panic(err.Error())
	}
	um := handlers.NewUserModule(
		conf.App.Auth.AllowEmptyPsk,
		conf.App.User.Register,
		conf.App.Auth.metaFieldLogin,
		ud,
	)

	router := mux.NewRouter()

	um.LoadRoutes(router.PathPrefix("/u").Subrouter())

	var handler http.Handler = router

	if len(cors) != 0 {
		logger.Debug("Applying CORS Middleware")
		handler = handlers.MiddlewareCORS(handler, cors)
	}

	return handlers.MiddlewareLogger(handler, logger)
}

func main() {
	logger, _ = log.NewConsole(log.LevelDebg, false)

	args := parseArgs()
	logger.Level = args.GetLogLevel()

	conf, err := loadConfig(args.ConfigFile)
	if err != nil {
		logger.Panic(err.Error())
	}

	// Connect to Mongo database
	if err := database.Connect(
		conf.Database.MongoDB.DSN,
		conf.Database.MongoDB.Database,
	); err != nil {
		logger.Panic(err.Error())
	} else {
		logger.Debug("MongoDB is connected")
	}

	// TODO: Create RedisDB connection
	// NOTE: Validate is app.modules.session is active

	// Migrating data (if migration file is set)
	migrate(args.MigrationFile)

	server = &http.Server{
		Addr: *args.Publish,
		Handler: createRouter(
			conf.Server.CORSDomain,
			conf,
		),
		WriteTimeout: conf.ServerTTL(),
		ReadTimeout:  conf.ServerTTL(),
	}

	logger.ImportantInfo("Server listening on: %#v", *args.Publish)
	if err := server.ListenAndServe(); err != nil {
		logger.Panic(err.Error())
	}
}
