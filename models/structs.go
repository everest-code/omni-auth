package models

import (
	"fmt"
	"strings"
	"time"

	"gitlab.com/everest-code/omni-auth/utils"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type (
	AppToken struct {
		// Identification
		ID    primitive.ObjectID `bson:"_id,omitempty" json:"id,omitempty"`
		Token []byte             `bson:"token" json:"-"`

		// CORS Domain access
		Origin string `bson:"origin" json:"origin"`
		Name   string `bson:"name" json:"name"`

		// Access data
		Admin  bool   `bson:"admin" json:"administrative"`
		secret string `bson:"secret" json:"-"`
	}

	User struct {
		// Identification
		ID        primitive.ObjectID `bson:"_id,omitempty" json:"id,omitempty"`
		UserToken string             `bson:"user_token" json:"userToken"`

		// Access data and tagging
		Meta  map[string]string `bson:"meta" json:"__meta__"`
		Tags  []string          `bson:"tags" json:"tags"`
		Admin bool              `bson:"admin" json:"administrative"`

		// Internal api metadata
		CreatedOnApp primitive.ObjectID `bson:"created_on" json:"createdOn"`
		CreatedAt    time.Time          `bson:"created_at" json:"createdAt"`
		LastLoginAt  *time.Time         `bson:"last_login" json:"-"`

		password []byte `bson:"password" json:"-"`
	}

	Permission struct {
		ID          primitive.ObjectID `bson:"_id,omitempty" json:"-"`
		Module      string             `bson:"module" json:"module"`
		Action      string             `bson:"action" json:"name"`
		Description string             `bson:"description" json:"description,omitempty"`
		// TODO: Implement meta for v2
		// Meta map[string]string `bson:"meta" json:"__meta__"`
	}

	Role struct {
		ID          primitive.ObjectID `bson:"_id,omitempty" json:"-"`
		DefaultRole bool               `bson:"default" json:"-"`
		Name        string             `bson:"name" json:"name"`
		Description string             `bson:"description" json:"description,omitempty"`
		PermsRegex  []string           `bson:"perms" json:"-"`
		// TODO: Implement meta for v2
		// Meta map[string]string `bson:"meta" json:"__meta__"`
	}

	// User <===> Role relation (many to many)
	// NOTE: Only for MongoDB relation use
	UserRole struct {
		User primitive.ObjectID `bson:"u"`
		Role primitive.ObjectID `bson:"r"`
	}
)

func (p Permission) String() string {
	return fmt.Sprintf("%s.%s", p.Module, p.Action)
}

// Check if the permission given is valid with the actual permission.
func (p Permission) IsPermission(perm string) bool {
	return p.String() == perm
}

// Check if the given role is in the permission regex
func (r Role) PermInRole(perm Permission) bool {
	for _, re := range r.PermsRegex {
		if utils.NewStringMatch(re).Match(perm.String()) {
			return true
		}
	}

	return false
}

// List permissions in the role
func (r Role) FilterPermissions(perms []Permission) []Permission {
	res := make([]Permission, 0)

	for _, perm := range perms {
		if r.PermInRole(perm) {
			res = append(res, perm)
		}
	}

	return res
}

func (r Role) String() string {
	return strings.TrimSpace(fmt.Sprintf("(%s) %s", r.Name, r.Description))
}
