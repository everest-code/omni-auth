package database

import (
	"context"
	"errors"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var (
	db  *mongo.Database
	ctx context.Context = context.TODO()
)

// Create a new connection from a DSN string
func createConnection(dsn string) (*mongo.Client, error) {
	opts := options.Client().ApplyURI(dsn)

	conn, err := mongo.Connect(ctx, opts)
	if err != nil {
		return nil, err
	}

	return conn, nil
}

// Get database of a connection
func GetDB(dsn, database string) (*mongo.Database, error) {
	if db == nil {
		conn, err := createConnection(dsn)
		if err != nil {
			return nil, err
		}

		db = conn.Database(database)
	}

	return db, nil
}

// Connect with database, without returning the Database
// NOTE: Only do a ping with the database
func Connect(dsn, database string) error {
	db, err := GetDB(dsn, database)
	if err != nil {
		return err
	}

	if err := db.Client().Ping(ctx, nil); err != nil {
		return err
	}

	return nil
}

// Get a collection from the database
// WARN: First use database.Connect
// NOTE: The collection getting don't specify if the database is ready to get data
func Coll(coll string) (*mongo.Collection, error) {
	if db == nil {
		return nil, errors.New("database is not connected, run database.Connect first")
	}

	return db.Collection(coll), nil
}
