package database

import (
	"io/ioutil"
	"os"

	"gitlab.com/everest-code/omni-auth/models"
	"gopkg.in/yaml.v2"
)

type Migration struct {
	Permissions []struct {
		Module string `yaml:"module"`
		Perms  []struct {
			Name        string `yaml:"name"`
			Description string `yaml:"desc"`
		} `yaml:"perms"`
	} `yaml:"perms"`

	Roles []struct {
		Name        string   `yaml:"name"`
		Description string   `yaml:"desc"`
		Default     bool     `yaml:"def"`
		PermRegex   []string `yaml:"perms"`
	} `yaml:"roles"`
}

// Load a migration object from a YAML file
func LoadMigrationFile(f *os.File) (*Migration, error) {
	defer f.Close()
	data, err := ioutil.ReadAll(f)
	if err != nil {
		return nil, err
	}

	migration := new(Migration)
	err = yaml.Unmarshal(data, migration)
	return migration, err
}

// Cast models.Permission from Migration
func (m *Migration) ToPerms() []models.Permission {
	perms := make([]models.Permission, 0)
	for _, mod := range m.Permissions {
		for _, perm := range mod.Perms {
			perms = append(perms, models.Permission{
				Module:      mod.Module,
				Action:      perm.Name,
				Description: perm.Description,
			})
		}
	}

	return perms
}

// Cast models.Role from Migration
func (m *Migration) ToRoles() []models.Role {
	roles := make([]models.Role, 0)
	for _, role := range m.Roles {
		roles = append(roles, models.Role{
			Name:        role.Name,
			Description: role.Description,
			DefaultRole: role.Default,
			PermsRegex:  role.PermRegex,
		})
	}
	return roles
}
