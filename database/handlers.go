package database

import (
	"errors"

	"gitlab.com/everest-code/omni-auth/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type (
	DBInit interface {
		SetDatabase(*mongo.Collection)
		GetCollection() string
	}

	RoleHandler struct {
		db *mongo.Collection
	}

	PermissionHandler struct {
		db *mongo.Collection
	}

	UserHandler struct {
		db *mongo.Collection
	}
)

var (
	NotInitialized error = errors.New("the handler was not initialized, please use InitDatabase(handler)")
	NotFound       error = errors.New("model was not found")
	IdParseErr     error = errors.New("error parsing ID")
)

// Set a mongo collection from empty DBInit
func InitDatabase(handler DBInit) error {
	coll, err := Coll(handler.GetCollection())
	if err != nil {
		return err
	}

	handler.SetDatabase(coll)
	return nil
}

/////////////////////
//// Roles funcs ////
/////////////////////

func (r *RoleHandler) SetDatabase(db *mongo.Collection) {
	r.db = db
}

func (r RoleHandler) GetCollection() string {
	return "roles"
}

func (r *RoleHandler) ensureDB() error {
	if r.db == nil {
		return NotInitialized
	}
	return nil
}

// Get a Role by name
func (r *RoleHandler) GetByName(name string) (*models.Role, error) {
	if err := r.ensureDB(); err != nil {
		return nil, err
	}

	var res *models.Role
	err := r.db.FindOne(ctx, bson.D{
		{"name", name},
	}).Decode(res)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, NotFound
		}
		return nil, err
	}

	return res, nil
}

// Get all roles
func (r *RoleHandler) GetAll() ([]models.Role, error) {
	if err := r.ensureDB(); err != nil {
		return nil, err
	}

	cur, err := r.db.Find(ctx, bson.D{}, nil)
	if err != nil {
		if err == mongo.ErrNilDocument {
			return make([]models.Role, 0), nil
		}
		return nil, err
	}

	res := make([]models.Role, 0)
	for cur.Next(ctx) {
		i := new(models.Role)
		if err := cur.Decode(i); err != nil {
			return nil, err
		}
		res = append(res, *i)
	}

	return res, nil
}

// Create a role
func (r *RoleHandler) Create(role *models.Role) error {
	if err := r.ensureDB(); err != nil {
		return err
	}

	if role.ID != primitive.NilObjectID {
		return errors.New("role.ID must be nil")
	}

	id, err := r.db.InsertOne(ctx, role)
	if err != nil {
		return err
	}

	if nid, ok := id.InsertedID.(primitive.ObjectID); !ok {
		return IdParseErr
	} else {
		role.ID = nid
	}
	return nil
}

//////////////////////////
//// Permission funcs ////
//////////////////////////

func (r *PermissionHandler) SetDatabase(db *mongo.Collection) {
	r.db = db
}

func (r PermissionHandler) GetCollection() string {
	return "perms"
}

func (r *PermissionHandler) ensureDB() error {
	if r.db == nil {
		return NotInitialized
	}
	return nil
}

// Get a Permission by name
func (r *PermissionHandler) GetByName(module, action string) (*models.Permission, error) {
	if err := r.ensureDB(); err != nil {
		return nil, err
	}

	var res *models.Permission
	err := r.db.FindOne(ctx, bson.D{
		{"module", module},
		{"action", action},
	}).Decode(res)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, NotFound
		}
		return nil, err
	}

	return res, nil
}

// Get all perms
func (r *PermissionHandler) GetAll() ([]models.Permission, error) {
	if err := r.ensureDB(); err != nil {
		return nil, err
	}

	cur, err := r.db.Find(ctx, bson.D{}, nil)
	if err != nil {
		if err == mongo.ErrNilDocument {
			return make([]models.Permission, 0), nil
		}
		return nil, err
	}

	res := make([]models.Permission, 0)
	for cur.Next(ctx) {
		i := new(models.Permission)
		if err := cur.Decode(i); err != nil {
			return nil, err
		}
		res = append(res, *i)
	}

	return res, nil
}

// Create a perm
func (r *PermissionHandler) Create(perm *models.Permission) error {
	if err := r.ensureDB(); err != nil {
		return err
	}

	if perm.ID != primitive.NilObjectID {
		return errors.New("perm.ID must be nil")
	}

	id, err := r.db.InsertOne(ctx, perm)
	if err != nil {
		return err
	}

	if nid, ok := id.InsertedID.(primitive.ObjectID); !ok {
		return IdParseErr
	} else {
		perm.ID = nid
	}
	return nil
}

//////////////////////
//// User handler ////
//////////////////////

func (u *UserHandler) SetDatabase(db *mongo.Collection) {
	u.db = db
}

func (u UserHandler) GetCollection() string {
	return "users"
}

func (u *UserHandler) ensureDB() error {
	if u.db == nil {
		return NotInitialized
	}
	return nil
}
