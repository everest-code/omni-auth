module gitlab.com/everest-code/omni-auth

go 1.16

require (
	github.com/akamensky/argparse v1.3.1
	github.com/c2h5oh/datasize v0.0.0-20200825124411-48ed595a09d2
	github.com/gorilla/mux v1.8.0
	github.com/pelletier/go-toml v1.9.4
	gitlab.com/everest-code/golib/log v1.0.1
	go.mongodb.org/mongo-driver v1.8.4
	gopkg.in/yaml.v2 v2.4.0
)
