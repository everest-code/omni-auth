package main

import (
	"io/ioutil"
	"os"
	"time"

	"github.com/pelletier/go-toml"
	"gitlab.com/everest-code/golib/log"
)

type (
	Conf struct {
		Server struct {
			CORSDomain []string `toml:"corsDomains"`
			Timeout    string   `toml:"timeout"`
		} `toml:"server"`
		App struct {
			Auth struct {
				AllowEmptyPsk  bool   `toml:"allowEmptyPassword"`
				metaFieldLogin string `toml:"metaFieldLogin"`
			} `toml:"auth"`
			Session struct {
				TTL             string `toml:"TTL"`
				ResetOnAction   bool   `toml:"resetTTLOnAction"`
				ValueSerializer string `toml:"valueSerializer"`
			} `toml:"session"`
			User struct {
				Register bool `toml:"register"`
			} `toml:"user"`
			Modules map[string]bool `toml:"modules"`
		} `toml:"app"`
		Database struct {
			MongoDB struct {
				DSN      string `toml:"DSN"`
				Database string `toml:"database"`
			} `toml:"mongo"`
			RedisDB struct {
				Addr     string `toml:"address"`
				Password string `toml:"password"`
				DB       uint8  `toml:"db"`
			} `toml:"redis"`
		} `toml:"database"`
	}
)

func loadConfig(f *os.File) (*Conf, error) {
	defer f.Close()

	data, err := ioutil.ReadAll(f)
	if err != nil {
		return nil, err
	}

	conf := new(Conf)

	err = toml.Unmarshal(data, conf)
	return conf, err
}

func (c Conf) ServerTTL() time.Duration {
	d, err := time.ParseDuration(c.Server.Timeout)
	if err != nil {
		log.Panic(err.Error())
	}
	return d
}
