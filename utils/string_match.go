package utils

import (
	"regexp"
	"strings"
)

type StringMatch struct {
	Negate bool
	Regex  *regexp.Regexp
}

func NewStringMatch(match string) *StringMatch {
	negate := strings.HasPrefix(match, "!")
	if negate {
		match = match[1 : len(match)-1]
	}

	match = strings.ReplaceAll(strings.ReplaceAll(strings.ReplaceAll(match, ".", "\\."), "*", ".+"), "?", ".")

	return &StringMatch{
		Negate: negate,
		Regex:  regexp.MustCompile(match),
	}
}

func (s StringMatch) negate(t bool) bool {
	if s.Negate {
		return !t
	}
	return t
}

func (s StringMatch) Match(t string) bool {
	return s.negate(s.Regex.MatchString(t))
}
