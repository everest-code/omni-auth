package handlers

import (
	"net/http"
	"strings"
	"time"

	"github.com/c2h5oh/datasize"
	"gitlab.com/everest-code/golib/log"
)

type ResponseLogger struct {
	InnerRes   http.ResponseWriter
	Size       int
	StatusCode int
}

func (r *ResponseLogger) Write(data []byte) (int, error) {
	l, err := r.InnerRes.Write(data)
	if err != nil {
		return 0, err
	}

	r.Size += l
	return l, nil
}

func (r *ResponseLogger) WriteHeader(statusCode int) {
	r.StatusCode = statusCode
	r.InnerRes.WriteHeader(statusCode)
}

func (r *ResponseLogger) Header() http.Header {
	return r.InnerRes.Header()
}

func MiddlewareLogger(next http.Handler, logger *log.Logger) http.Handler {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		nRes := &ResponseLogger{
			InnerRes:   res,
			Size:       0,
			StatusCode: 200,
		}

		start := time.Now()
		next.ServeHTTP(nRes, req)
		diff := time.Now().Sub(start)
		logger.Info("[%s %s] %d %s %s", req.Method, req.URL.Path, nRes.StatusCode, datasize.ByteSize(nRes.Size), diff)
	})
}

func MiddlewareCORS(next http.Handler, domains []string) http.Handler {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		res.Header().Add("Access-Control-Request-Method", "OPTIONS, GET, POST, DELETE")
		res.Header().Add("Access-Control-Allow-Origin", strings.Join(domains, ", "))
		res.Header().Add("Access-Control-Allow-Credentials", "true")
		res.Header().Add("Access-Control-Allow-Headers", "X-Session-Id, X-User-Token")
		res.Header().Add("Access-Control-Max-Age", "120")
		if req.Method == http.MethodOptions {
			res.WriteHeader(204)
			return
		}

		next.ServeHTTP(res, req)
	})
}
