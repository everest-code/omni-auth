package handlers

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/everest-code/omni-auth/database"
)

type UserModule struct {
	AllowEmptyPassword bool
	AuthField          string
	AllowRegister      bool

	db *database.UserHandler
}

func NewUserModule(allowEmpty bool, register bool, authField string, database *database.UserHandler) *UserModule {
	return &UserModule{
		AllowEmptyPassword: allowEmpty,
		AuthField:          authField,
		AllowRegister:      register,
		db:                 database,
	}
}

func (m *UserModule) LoadRoutes(r *mux.Router) {
	if m.AllowRegister {
		r.HandleFunc("/register", m.registerController).Methods("POST")
	}
}

func (m *UserModule) registerController(res http.ResponseWriter, req *http.Request) {
	fmt.Fprint(res, "Hello guy, and welcome.")
}
