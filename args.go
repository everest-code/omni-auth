package main

import (
	"fmt"
	"os"

	"github.com/akamensky/argparse"
	"gitlab.com/everest-code/golib/log"
)

type AppArgs struct {
	Publish       *string
	ConfigFile    *os.File
	MigrationFile *os.File
	LogLevel      *string
}

func parseArgs() (args *AppArgs) {
	args = new(AppArgs)
	parser := argparse.NewParser(os.Args[0], "REST API for authentication & user managing")

	args.Publish = parser.String("p", "publish", &argparse.Options{
		Required: false,
		Help:     "Publish ip:port",
		Default:  "0.0.0.0:8000",
	})

	args.ConfigFile = parser.File("c", "config", os.O_RDONLY, 0640, &argparse.Options{
		Required: true,
		Help:     "Configuration file (if not created use /etc/omniauth/conf.toml)",
	})

	args.MigrationFile = parser.File("m", "migration", os.O_RDONLY, 0640, &argparse.Options{
		Required: false,
		Help:     "Migration file (not required if permission module disabled)",
		Default:  nil,
	})

	args.LogLevel = parser.Selector("v", "verbosity", []string{
		"DEBUG",
		"INFO",
		"WARNING",
		"ERROR",
	}, &argparse.Options{
		Required: false,
		Help:     "Level of verbosity",
		Default:  "INFO",
	})

	err := parser.Parse(os.Args)
	if err != nil {
		fmt.Print(parser.Usage(err))
		os.Exit(1)
	}

	return
}

func (app AppArgs) GetLogLevel() log.Level {
	switch *app.LogLevel {
	case "DEBUG":
		return log.LevelDebg
	case "INFO":
		return log.LevelInfo
	case "WARNING":
		return log.LevelWarn
	case "ERROR":
		fallthrough
	default:
		return log.LevelErro
	}
}
